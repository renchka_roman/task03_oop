package com.epam.course.renchka.bean.department.buildmaterial;

import com.epam.course.renchka.bean.Product;

public class Sealant extends Product {

  public Sealant(String name, int price) {
    super(name, price);
  }
}
