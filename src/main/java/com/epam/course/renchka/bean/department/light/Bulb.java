package com.epam.course.renchka.bean.department.light;

import com.epam.course.renchka.bean.Product;

public class Bulb extends Product {

  public Bulb(String name, int price) {
    super(name, price);
  }
}
