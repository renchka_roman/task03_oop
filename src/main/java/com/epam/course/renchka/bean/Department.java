package com.epam.course.renchka.bean;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface Department {

  void addNewPosition(String name, List<Product> productList);

  Set<String> getPositions();

  List<Product> getProductList(String name);

  Map<String, List<Product>> getMap();
}
