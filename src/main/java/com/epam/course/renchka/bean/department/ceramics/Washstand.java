package com.epam.course.renchka.bean.department.ceramics;

import com.epam.course.renchka.bean.Product;

public class Washstand extends Product {

  public Washstand(String name, int price) {
    super(name, price);
  }
}
