package com.epam.course.renchka.bean.department.ceramics;

import com.epam.course.renchka.bean.Product;

public class Toilet extends Product {

  public Toilet(String name, int price) {
    super(name, price);
  }
}
