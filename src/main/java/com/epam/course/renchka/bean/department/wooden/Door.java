package com.epam.course.renchka.bean.department.wooden;

import com.epam.course.renchka.bean.Product;

public class Door extends Product {

  public Door(String name, int price) {
    super(name, price);
  }
}
