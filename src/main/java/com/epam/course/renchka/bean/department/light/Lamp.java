package com.epam.course.renchka.bean.department.light;

import com.epam.course.renchka.bean.Product;

public class Lamp extends Product {

  public Lamp(String name, int price) {
    super(name, price);
  }
}
