package com.epam.course.renchka.bean.department.wooden;

import com.epam.course.renchka.bean.Product;

public class Floor extends Product {

  public Floor(String name, int price) {
    super(name, price);
  }
}
