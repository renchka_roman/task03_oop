package com.epam.course.renchka.bean.department.wooden;

import com.epam.course.renchka.bean.Product;

public class Laminate extends Product {

  public Laminate(String name, int price) {
    super(name, price);
  }
}
