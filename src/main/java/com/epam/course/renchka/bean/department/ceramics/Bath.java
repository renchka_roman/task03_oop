package com.epam.course.renchka.bean.department.ceramics;

import com.epam.course.renchka.bean.Product;

public class Bath extends Product {

  public Bath(String name, int price) {
    super(name, price);
  }
}
