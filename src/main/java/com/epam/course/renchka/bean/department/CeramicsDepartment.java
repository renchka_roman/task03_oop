package com.epam.course.renchka.bean.department;

import com.epam.course.renchka.bean.Department;
import com.epam.course.renchka.bean.Product;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CeramicsDepartment implements Department {

  private String name;
  private Map<String, List<Product>> positions = new LinkedHashMap<>();

  public CeramicsDepartment(String name) {
    this.name = name;
  }

  public void addNewPosition(String name, List<Product> productList) {
    positions.put(name, productList);
  }

  @Override
  public Set<String> getPositions() {
    return positions.keySet();
  }

  public Map<String, List<Product>> getMap() {
    return positions;
  }

  public void removePosition(String name) {
    positions.remove(name);
  }

  public List<Product> getProductList(String name) {
    return positions.get(name);
  }

  public void addProductToProductList(String name, Product product) {
    getProductList(name).add(product);
  }

  public void removeProductFromProductList(String name, Product product) {
    getProductList(name).remove(product);
  }

  @Override
  public String toString() {
    return name;
  }
}
