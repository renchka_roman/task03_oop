package com.epam.course.renchka.bean.department.buildmaterial;

import com.epam.course.renchka.bean.Product;

public class Cement extends Product {

  public Cement(String name, int price) {
    super(name, price);
  }
}
