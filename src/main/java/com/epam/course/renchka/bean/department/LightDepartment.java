package com.epam.course.renchka.bean.department;

import com.epam.course.renchka.bean.Department;
import com.epam.course.renchka.bean.Product;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class LightDepartment implements Department {

  private String name;
  private Map<String, List<Product>> positions = new LinkedHashMap<>();

  public LightDepartment(String name) {
    this.name = name;
  }

  @Override
  public Map<String, List<Product>> getMap() {
    return positions;
  }

  @Override
  public void addNewPosition(String name, List<Product> productList) {
    positions.put(name, productList);
  }

  @Override
  public Set<String> getPositions() {
    return positions.keySet();
  }

  @Override
  public List<Product> getProductList(String name) {
    return null;
  }

  @Override
  public String toString() {
    return name;
  }
}
