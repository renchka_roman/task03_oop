package com.epam.course.renchka.bean.department.buildmaterial;

import com.epam.course.renchka.bean.Product;

public class Paint extends Product {

  public Paint(String name, int price) {
    super(name, price);
  }
}
