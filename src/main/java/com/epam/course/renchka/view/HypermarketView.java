package com.epam.course.renchka.view;

import com.epam.course.renchka.controller.HypermarketController;
import java.io.IOException;

public interface HypermarketView {

  void showMenu();

  void setHypermarketController(
      HypermarketController hypermarketController);

  void showDepartmentMenu(int number);

  void search(int departmentNum) throws IOException;
}
