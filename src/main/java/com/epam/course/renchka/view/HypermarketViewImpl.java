package com.epam.course.renchka.view;

import com.epam.course.renchka.bean.Product;
import com.epam.course.renchka.bean.Department;
import com.epam.course.renchka.controller.HypermarketController;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

public class HypermarketViewImpl implements HypermarketView {

  private HypermarketController hypermarketController;
  private static BufferedReader input =
      new BufferedReader(new InputStreamReader(System.in));

  @Override
  public void setHypermarketController(
      HypermarketController hypermarketController) {
    this.hypermarketController = hypermarketController;
  }

  @Override
  public void showMenu() {
    String choice = "";
    do {
      System.out.println("---------------------------------");
      System.out.println("HYPERMARKET");
      System.out.println("---------------------------------");
      List<Department> departmentList =
          hypermarketController.getDepartmentList();
      for (int i = 0; i < departmentList.size(); i++) {
        Department department = departmentList.get(i);
        System.out.println(i + ". " + department.toString().toUpperCase());

        for (String position : department.getPositions()) {
          System.out.println("\t* " + position);
        }
      }
      System.out.println("---------------------------------");
      System.out.println("Q - Вихід");
      System.out.println("---------------------------------");
      System.out.println("Виберіть відділ, який хочете переглянути");
      try {
        choice = input.readLine();
        if (choice.matches("[0-9]*")) {
          int number = Integer.parseInt(choice);
          if (number >= 0 && number < departmentList.size()) {
            showDepartmentMenu(number);
            choice = "Q";
          } else {
            System.out.println("зробіть правильний вибір");
          }
        }
      } catch (IOException e) {
        System.out.println("Повторіть спробу!");
      }
    } while (!choice.equals("Q"));
  }

  @Override
  public void showDepartmentMenu(int number) {
    String choice = "";
    do {
      Department department =
          hypermarketController.getDepartmentList().get(number);
      System.out.println("---------------------------------");
      System.out.println(department.toString().toUpperCase());
      System.out.println("---------------------------------");
      Map<String, List<Product>> map = department.getMap();
      int i = 0;
      for (Map.Entry<String, List<Product>> pair : map.entrySet()) {
        System.out.println(i + ". " + pair.getKey().toUpperCase());
        List<Product> productList = pair.getValue();
        for (Product product : productList) {
          System.out.println("\t* " + product);
        }
        i++;
      }
      System.out.println("---------------------------------");
      System.out.println("S - Пошук");
      System.out.println("B - Повернутися в поперднє меню");
      System.out.println("Q - Вихід");
      System.out.println("---------------------------------");
      try {
        choice = input.readLine();
        switch (choice) {
          case "B":
            showMenu();
            choice = "Q";
            break;
          case "S":
            search(number);
            choice = "Q";
            break;
        }
      } catch (IOException e) {
        System.out.println("Повторіть спробу!");
      }
    } while (!choice.equals("Q"));
  }

  @Override
  public void search(int departmentNum) throws IOException {
    String choice = "";
    do {
      System.out.println("Введіть номер позиції, де буде здійснюватись пошук:");
      int number = Integer.parseInt(input.readLine());
      System.out.println("Введіть суму:");
      double sum = Double.parseDouble(input.readLine());
      System.out.println(
          "Вивести товар більший(>), менший(<) чи рівний(=) вказаній сумі?"
              + "(потрібно ввести '>','<' або '=')");
      String str = input.readLine();
      List<Product> searchList =
          hypermarketController.search(departmentNum, number, sum, str);
      System.out.println("---------------------------------");
      for (Product product : searchList) {
        System.out.println(product);
      }
      System.out.println("---------------------------------");
      System.out.println("B - Повернутися в поперднє меню");
      System.out.println("Q - Вихід");
      System.out.println("---------------------------------");
      choice = input.readLine();
      if (choice.equals("B")) {
        showDepartmentMenu(departmentNum);
        choice = "Q";
      }
    } while (!choice.equals("Q"));
  }
}
