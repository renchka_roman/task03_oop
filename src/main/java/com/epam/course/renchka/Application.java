package com.epam.course.renchka;

import com.epam.course.renchka.controller.HypermarketController;
import com.epam.course.renchka.controller.HypermarketControllerImpl;
import com.epam.course.renchka.model.Domain;
import com.epam.course.renchka.model.HypermarketModel;
import com.epam.course.renchka.model.HypermarketModelImpl;
import com.epam.course.renchka.view.HypermarketView;
import com.epam.course.renchka.view.HypermarketViewImpl;

public class Application {

  public static void main(String[] args) {
    HypermarketModel hypermarketModel = new HypermarketModelImpl(new Domain());
    HypermarketController hypermarketController = new HypermarketControllerImpl();
    hypermarketController.setHypermarketModel(hypermarketModel);
    HypermarketView hypermarketView = new HypermarketViewImpl();
    hypermarketView.setHypermarketController(hypermarketController);


    hypermarketView.showMenu();
  }

}
