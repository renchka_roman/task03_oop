package com.epam.course.renchka.controller;

import com.epam.course.renchka.bean.Product;
import com.epam.course.renchka.bean.Department;
import com.epam.course.renchka.model.HypermarketModel;
import java.util.List;

public interface HypermarketController {

  void setHypermarketModel(HypermarketModel hypermarketModel);

  List<Department> getDepartmentList();

  List<Product> search(
      int departmentNum, int position, double sum, String searchParameter);
}
