package com.epam.course.renchka.controller;

import com.epam.course.renchka.bean.Product;
import com.epam.course.renchka.bean.Department;
import com.epam.course.renchka.model.HypermarketModel;
import java.util.List;

public class HypermarketControllerImpl implements HypermarketController {

  private HypermarketModel hypermarketModel;

  @Override
  public void setHypermarketModel(HypermarketModel hypermarketModel) {
    this.hypermarketModel = hypermarketModel;
  }

  @Override
  public List<Product> search(
      int departmentNum, int position, double sum, String searchParameter) {
    return hypermarketModel.search(
        departmentNum, position, sum, searchParameter);
  }

  @Override
  public List<Department> getDepartmentList() {
    return hypermarketModel.getDepartmentList();
  }
}
