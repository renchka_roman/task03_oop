package com.epam.course.renchka.model;

import com.epam.course.renchka.bean.Product;
import com.epam.course.renchka.bean.Department;
import java.util.ArrayList;
import java.util.List;

public class HypermarketModelImpl implements HypermarketModel {

  private Domain domain;

  @Override
  public void setDomain(Domain domain) {
    this.domain = domain;
  }

  @Override
  public List<Department> getDepartmentList() {
    return domain.getDepartmentList();
  }

  @Override
  public List<Product> search(
      int departmentNum, int position, double sum, String searchParameter) {
    List<Product> productList = domain.search(departmentNum, position);
    List<Product> searchProductList = new ArrayList<>();
    for (Product product : productList) {
      switch (searchParameter) {
        case ">":
          if (product.getPrice() > sum) {
            searchProductList.add(product);
          }
          break;
        case "<":
          if (product.getPrice() < sum) {
            searchProductList.add(product);
            break;
          }
        case "=":
          if (product.getPrice() == sum) {
            searchProductList.add(product);
            break;
          }
      }
    }
    return searchProductList;
  }
}
