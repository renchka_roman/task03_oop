package com.epam.course.renchka.model;

import com.epam.course.renchka.bean.*;
import com.epam.course.renchka.bean.department.*;
import com.epam.course.renchka.bean.department.buildmaterial.*;
import com.epam.course.renchka.bean.department.ceramics.*;
import com.epam.course.renchka.bean.department.light.*;
import com.epam.course.renchka.bean.department.wooden.*;
import java.util.ArrayList;
import java.util.List;

public class Domain {

  private List<Department> departments = new ArrayList<>();

  public List<Department> getDepartmentList() {
    if (departments.size() == 0) {
      createDepartments();
    }
    return departments;
  }

  public List<Product> search(int departmentNumber, int position) {
    Department department = departments.get(departmentNumber);
    String[] positions = department.getPositions().toArray(new String[]{});
    return department.getMap().get(positions[position]);
  }

  private void createDepartments() {
    departments.add(createCeramicDepartment());
    departments.add(createBuildMaterialsDepartment());
    departments.add(createLightingDepartment());
    departments.add(createWoodenProductsDepartment());
  }

  private Department createCeramicDepartment() {
    Department ceramics = new CeramicsDepartment("Ceramics");
    ceramics.addNewPosition("Baths", createBathsPosition());
    ceramics.addNewPosition("Toilets", createToiletsPosition());
    ceramics.addNewPosition("Washstands", createWashstandsPosition());
    return ceramics;
  }

  private Department createBuildMaterialsDepartment() {
    Department buildMaterials =
        new BuildMaterialsDepartment("Building Materials");
    buildMaterials.addNewPosition("Cement", createCementPosition());
    buildMaterials.addNewPosition("Paints", createPaintPosition());
    buildMaterials.addNewPosition("Sealants", createSealantPosition());
    return buildMaterials;
  }

  private Department createLightingDepartment() {
    Department lighting = new LightDepartment("Lighting");
    lighting.addNewPosition("Bulbs", createBulbsPosition());
    lighting.addNewPosition("Lamps", createLampsPosition());
    lighting.addNewPosition("Sockets", createSocketsPosition());
    return lighting;
  }

  private Department createWoodenProductsDepartment() {
    Department woodenMaterials =
        new WoodenMaterialDepartment("Wooden products");
    woodenMaterials.addNewPosition("Doors", createDoorsPosition());
    woodenMaterials.addNewPosition("Floor", createFloorPosition());
    woodenMaterials.addNewPosition("Laminate", createLaminatePosition());
    return woodenMaterials;
  }

  private List<Product> createLaminatePosition() {
    List<Product> laminateList = new ArrayList<>();
    laminateList.add(new Laminate("Laminate1", 350));
    laminateList.add(new Laminate("Laminate2", 445));
    laminateList.add(new Laminate("Laminate3", 635));
    laminateList.add(new Laminate("Laminate4", 200));
    return laminateList;
  }

  private List<Product> createFloorPosition() {
    List<Product> floorList = new ArrayList<>();
    floorList.add(new Floor("Floor1", 950));
    floorList.add(new Floor("Floor2", 1550));
    floorList.add(new Floor("Floor3", 845));
    floorList.add(new Floor("Floor4", 1400));
    return floorList;
  }

  private List<Product> createDoorsPosition() {
    List<Product> doors = new ArrayList<>();
    doors.add(new Door("Door1", 3000));
    doors.add(new Door("Door2", 5000));
    doors.add(new Door("Door3", 4500));
    doors.add(new Door("Door4", 3950));
    return doors;
  }

  private List<Product> createSocketsPosition() {
    List<Product> sockets = new ArrayList<>();
    sockets.add(new Socket("Socket1", 70));
    sockets.add(new Socket("Socket2", 150));
    sockets.add(new Socket("Socket3", 90));
    sockets.add(new Socket("Socket4", 85));
    return sockets;
  }

  private List<Product> createLampsPosition() {
    List<Product> lamps = new ArrayList<>();
    lamps.add(new Lamp("Lamp1", 900));
    lamps.add(new Lamp("Lamp2", 300));
    lamps.add(new Lamp("Lamp3", 450));
    lamps.add(new Lamp("Lamp4", 650));
    return lamps;
  }

  private List<Product> createBulbsPosition() {
    List<Product> bulbs = new ArrayList<>();
    bulbs.add(new Bulb("Bulb1", 20));
    bulbs.add(new Bulb("Bulb2", 50));
    bulbs.add(new Bulb("Bulb3", 39));
    bulbs.add(new Bulb("Bulb4", 45));
    return bulbs;
  }

  private List<Product> createSealantPosition() {
    List<Product> sealants = new ArrayList<>();
    sealants.add(new Sealant("Sealant1", 99));
    sealants.add(new Sealant("Sealant2", 120));
    sealants.add(new Sealant("Sealant3", 150));
    sealants.add(new Sealant("Sealant4", 250));
    return sealants;
  }

  private List<Product> createPaintPosition() {
    List<Product> paints = new ArrayList<>();
    paints.add(new Paint("Paint1", 150));
    paints.add(new Paint("Paint2", 300));
    paints.add(new Paint("Paint3", 235));
    paints.add(new Paint("Paint4", 185));
    return paints;
  }

  private List<Product> createCementPosition() {
    List<Product> cementList = new ArrayList<>();
    cementList.add(new Cement("Cement1", 70));
    cementList.add(new Cement("Cement2", 90));
    cementList.add(new Cement("Cement3", 99));
    cementList.add(new Cement("Cement4", 75));
    return cementList;
  }

  private List<Product> createBathsPosition() {
    List<Product> baths = new ArrayList<>();
    baths.add(new Bath("Bath1", 2000));
    baths.add(new Bath("Bath2", 3670));
    baths.add(new Bath("Bath3", 2450));
    baths.add(new Bath("Bath4", 5500));
    return baths;
  }

  private List<Product> createToiletsPosition() {
    List<Product> toilets = new ArrayList<>();
    toilets.add(new Toilet("Toilet1", 800));
    toilets.add(new Toilet("Toilet2", 950));
    toilets.add(new Toilet("Toilet3", 1200));
    toilets.add(new Toilet("Туалет4", 999));
    return toilets;
  }

  private List<Product> createWashstandsPosition() {
    List<Product> washstands = new ArrayList<>();
    washstands.add(new Washstand("Washstand1", 1500));
    washstands.add(new Washstand("Washstand2", 1000));
    washstands.add(new Washstand("Washstand3", 2000));
    washstands.add(new Washstand("Washstand4", 1655));
    return washstands;
  }
}
