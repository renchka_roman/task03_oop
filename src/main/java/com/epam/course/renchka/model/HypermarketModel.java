package com.epam.course.renchka.model;

import com.epam.course.renchka.bean.Product;
import com.epam.course.renchka.bean.Department;
import java.util.List;

public interface HypermarketModel {

  void setDomain(Domain domain);

  List<Department> getDepartmentList();

  List<Product> search(
      int departmentNum, int position, double sum, String searchParameter);
}
